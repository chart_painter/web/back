# Introduction

# Deploy
## Local
- create virtualenv & install requirements
```bash
pyenv virtualenv 3.7.4 chart_painter
pyenv activate chart_painter
pip install poetry black
poetry install
``` 
- create DB in Postgres chart_painter
- add envs from [.env.dev](.env.dev)
- you can create ```project/settings_local.py``` if it necessary
- apply DB's migrations ```./manage.py migrate```
- create super user ```./manage.py create_super_user```
- runserver ```./manage.py runserver```
- open [localhost:8000/admin/](http://localhost:8000/admin/)

## Running tests
To running tests deploy the project according to point above and add to envs ```ROLE=tests```
