isort:
	@echo -n "Run isort"
	isort -rc .

.black: $(shell find . -type d)
	black -l 120 .
	@if ! isort -c -rc .; then \
		echo "Import sort errors, run 'make isort' to fix them!!!"; \
		isort --diff -rc .; \
		false; \
	fi

.PHONY: black
black: .black
	@echo -n "Run black and isort"
	black .
