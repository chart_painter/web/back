#!/usr/bin/env bash

args=("$@")

case "${1}" in
    "bash")
        shift
        exec bash -c "${args[@]:1}"
        ;;
    "run-back")
        ./wait-for-it.sh pg:5432 --timeout=30 --strict -- echo "Postgres is up"
        exec gunicorn project.wsgi:application -w 4 --log-level=info --bind=0.0.0.0:8000
        ;;
    "run-celery")
        ./wait-for-it.sh pg:5432 --timeout=30 --strict -- echo "Postgres is up"
        ./wait-for-it.sh redis:6379 --timeout=30 --strict -- echo "Redis is up"
        exec celery worker -A project -E -Q charts --beat
        ;;
esac
