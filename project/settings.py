import os

import dj_database_url
import environ
from split_settings.tools import include, optional

include(optional("settings_local.py"))

root = environ.Path(__file__, "../..")

os.sys.path.insert(0, root())
os.sys.path.insert(0, os.path.join(root(), "apps"))

env = environ.Env(
    DEBUG=(bool, False),
    SECRET_KEY=(str, "dv&p_-a0m%v9c1#^y1l)24d^l29xlb70ps@-e1weflqiy*8fev"),
    ALLOWED_HOSTS=(list, ["*"]),
    DB_DSN=(str, None),
    TIME_ZONE=(str, "UTC"),
    LANGUAGE_CODE=(str, "en"),
    ROLE=(str, "prod"),
    BASE_URL=(str, "http://localhost:8000"),
    AMQP_DSN=(str, None),
    AMQP_EXCHANGE=(str, "charts"),
    AMQP_ROUTING_KEY=(str, "charts"),
    REDIS_URL=(str, None),
    BROKER_URL=(str, None),
    CELERY_BROKER_URL=(str, None),
    CELERY_RESULT_BACKEND=(str, None),
    CELERY_RESULT_BACKEND_URL=(str, None),
)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ROLE = env("ROLE")

SECRET_KEY = env("SECRET_KEY")
DEBUG = env("DEBUG")
ALLOWED_HOSTS = env("ALLOWED_HOSTS")

INSTALLED_APPS = [
    "solo",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "djoser",
    "django_filters",
    "rest_framework",
    "drf_yasg",
    "flex",
    "jsoneditor",
    "swagger_spec_validator",
    "rest_framework.authtoken",
    "apps.api.apps.ApiConfig",
    "apps.chart.apps.ChartConfig",
    "apps.common.apps.CommonConfig",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "project.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [root("project/templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "project.wsgi.application"

DATABASES = {"default": dj_database_url.config(default=env("DB_DSN"))}
FIXTURE_DIRS = [root("project/fixtures")]

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

USE_TZ = True
TIME_ZONE = env("TIME_ZONE")

LANGUAGE_CODE = env("LANGUAGE_CODE")
USE_I18N = True
USE_L10N = True

MEDIA_URL = "/media/"
MEDIA_ROOT = root("media")

STATIC_URL = "/static/"
STATIC_ROOT = root("static")
STATICFILES_DIRS = [root("project/static")]

BASE_URL = env("BASE_URL")

REST_FRAMEWORK_EXTENSIONS = {"DEFAULT_CACHE_RESPONSE_TIMEOUT": 1}

REST_FRAMEWORK = {
    "PAGE_SIZE": 100,
    "UPLOADED_FILES_USE_URL": env("ROLE") != "test",
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "apps.api.authentications.CsrfExemptSessionAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ],
    "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.IsAuthenticated"],
    "TEST_REQUEST_DEFAULT_FORMAT": "json",
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
    "DEFAULT_SCHEMA_CLASS": "rest_framework.schemas.coreapi.AutoSchema",
}

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

LOGIN_URL = "/admin/login/"

CORS_ORIGIN_ALLOW_ALL = True

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "root": {"level": "INFO", "handlers": []},
    "formatters": {
        "verbose": {"format": "%(levelname)s  %(asctime)s  %(module)s " "%(process)d  %(thread)d  %(message)s"}
    },
    "handlers": {"console": {"level": "INFO", "class": "logging.StreamHandler", "formatter": "verbose"},},
    "loggers": {
        "django.server": {"level": "INFO", "handlers": ["console"], "propagate": False},
        "django.db.backends": {"level": "INFO", "handlers": ["console"], "propagate": False},
    },
}

BROKER_URL = env("BROKER_URL")
CELERY_BROKER_URL = env("CELERY_BROKER_URL")
CELERY_RESULT_BACKEND = env("CELERY_RESULT_BACKEND_URL")
CELERY_ACCEPT_CONTENT = ["application/x-python-serialize"]
CELERY_TASK_SERIALIZER = "pickle"
CELERY_RESULT_SERIALIZER = "json"
CELERY_TIMEZONE = TIME_ZONE
CELERY_ENABLE_UTC = True
CELERY_APP = "project"
CELERYD_LOG_LEVEL = "INFO"
CELERYD_PID_FILE = "/tmp/celery-%I.pid"
CELERYD_OPTS = "--concurrency=8"
CELERYBEAT_OPTS = '--schedule="/tmp/celerybeat-schedule-%I"'

DJOSER = {"SERIALIZERS": {"token_create": "apps.api.user.serializers.TokenCreateSerializer"}}

AMQP_DSN = env("AMQP_DSN")
AMQP_EXCHANGE = env("AMQP_EXCHANGE")
AMQP_ROUTING_KEY = env("AMQP_ROUTING_KEY")

if ROLE == "test":

    class DisableMigrations(object):
        def __contains__(self, item):
            return True

        def __getitem__(self, item):
            return None

    MIGRATION_MODULES = DisableMigrations()

include(optional("settings_local.py"))
