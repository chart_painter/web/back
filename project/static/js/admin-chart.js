(function($) {
    $(document).ready(function () {
        const $element = $(".form-row.field-chart")
        $element.html("")

        if ($("input[name='status']:checked"). val() === "success") {
            try {
                const options = {
                    chart: {
                        height: 500,
                        type: 'line',
                    },
                    series: [{
                        name: "Value",
                        data: JSON.parse($("#initial-id_data").val()),
                    }],
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'straight'
                    },
                    title: {
                        text: 'Results of "y = A * x ^ 3 + B * x ^ 2 + C * x + D"',
                        align: 'left'
                    },
                    grid: {
                        row: {
                            colors: ['#f3f3f3', 'transparent'],
                            opacity: 0.5
                        },
                    },
                    xaxis: {
                        categories: [],
                    }
                }

                const stop = $('#id_max_x').val()
                const step = $('#id_step').val()
                options["xaxis"]["categories"] = Array(Math.ceil(stop / step)).fill(0).map((x, y) => x + y * step)

                options["series"]["data"] = JSON.parse($("#initial-id_data").val())
                const chart = new ApexCharts($element[0], options)
                chart.render()
            }
            catch (e) {
                console.log(e);
            }
        }
    });
})(django.jQuery);