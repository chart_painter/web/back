from django.urls import include, path

urlpatterns = [
    path("user/", include(("apps.api.user.urls", "apps.api"), namespace="user")),
    path("chart/", include(("apps.api.chart.urls", "apps.api"), namespace="chart")),
    path("docs/", include(("apps.api.docs.urls", "apps.api"), namespace="docs")),
]
