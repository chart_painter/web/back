from unittest import mock

from django.forms import model_to_dict
from django_dynamic_fixture import G, N
from rest_framework.reverse import reverse

from apps.api.base_test import BaseApiTest
from apps.api.chart import serializers
from apps.chart.models import Chart


@mock.patch("apps.chart.tasks.put_chart_to_queue.delay")
class ChartCreateTest(BaseApiTest):
    _url: str

    def setUp(self):
        super(BaseApiTest, self).setUp()

        self._url = reverse("api_v1:chart:chart-list")

    def test_success(self, m):
        self.client.force_login(self.user)

        post_data = self._post_data
        response = self.client.post(self._url, data=post_data)
        self.assertEqual(response.status_code, 201)

        count = Chart.objects.count()
        self.assertEqual(count, 1)

        instance = Chart.objects.last()
        instance_data = model_to_dict(instance, exclude=["id"])
        del instance_data["status"]
        self.assertDictEqual(instance_data, post_data)

    def test_un_auth(self, m):
        post_data = self._post_data
        response = self.client.post(self._url, data=post_data)
        self.assertEqual(response.status_code, 403)

        count = Chart.objects.count()
        self.assertEqual(count, 0)

        self.assertFalse(m.called)

    @property
    def _post_data(self) -> dict:
        return model_to_dict(N(Chart), exclude=serializers.ChartCreateSerializer.Meta.read_only_fields)


class ChartListTest(BaseApiTest):
    _url: str

    def setUp(self):
        super(BaseApiTest, self).setUp()

        self._url = reverse("api_v1:chart:chart-list")

    def test_success(self):
        for i in range(3):
            G(Chart)

        self.client.force_login(self.user)

        response = self.client.get(self._url)
        self.assertEqual(response.status_code, 200)

        self.assertListInResponse(serializers.ChartListSerializer, Chart.objects.all(), response, self.user)

    def test_un_auth(self):
        response = self.client.get(self._url)
        self.assertEqual(response.status_code, 403)


class ChartRetrieveTest(BaseApiTest):
    url_named = "api_v1:chart:chart-detail"

    def test_success(self):
        instance = G(Chart)

        self.client.force_login(self.user)

        url = self.get_detail_url(instance.pk)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertRetrieveInResponse(serializers.ChartRetrieveSerializer, instance, response, self.user)


class ChartUpdateTest(BaseApiTest):
    url_named = "api_v1:chart:chart-detail"

    def test_success(self):
        instance = G(Chart)

        self.client.force_login(self.user)

        url = self.get_detail_url(instance.pk)
        post_data = self._post_data
        response = self.client.patch(url, data=post_data)
        self.assertEqual(response.status_code, 200)

        instance = Chart.objects.last()

        self.assertRetrieveInResponse(serializers.ChartUpdateSerializer, instance, response, self.user)

        instance_data = model_to_dict(instance, fields=serializers.ChartUpdateSerializer.Meta.fields)
        self.assertDictEqual(instance_data, post_data)

    def test_un_auth(self):
        instance = G(Chart)

        url = self.get_detail_url(instance.pk)
        post_data = self._post_data
        response = self.client.patch(url, data=post_data)
        self.assertEqual(response.status_code, 403)

        instance = Chart.objects.last()
        instance_data = model_to_dict(instance, fields=serializers.ChartUpdateSerializer.Meta.fields)
        self.assertNotEqual(instance_data, post_data)

    @property
    def _post_data(self) -> dict:
        return model_to_dict(N(Chart), fields=serializers.ChartUpdateSerializer.Meta.fields)
