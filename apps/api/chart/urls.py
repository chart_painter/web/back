from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r"chart", views.ChartViewSet, basename="chart")

urlpatterns = router.urls
