from django.db import transaction
from rest_framework.viewsets import ModelViewSet

from apps.chart.models import Chart
from apps.chart.tasks import put_chart_to_queue

from ..views import SerializerViewSetMixin
from . import serializers

__all__ = ["ChartViewSet"]


class ChartViewSet(SerializerViewSetMixin, ModelViewSet):
    queryset = Chart.objects.all()
    serializer_class_map = {
        "create": serializers.ChartCreateSerializer,
        "list": serializers.ChartListSerializer,
        "retrieve": serializers.ChartRetrieveSerializer,
        "update": serializers.ChartUpdateSerializer,
        "partial_update": serializers.ChartUpdateSerializer,
    }

    def perform_create(self, serializer):
        instance: Chart = serializer.save()

        with transaction.atomic():
            transaction.on_commit(lambda: put_chart_to_queue.delay(instance.pk))
