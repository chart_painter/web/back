from drf_dynamic_fields import DynamicFieldsMixin
from rest_framework import serializers

from apps.chart import models

__all__ = ["ChartCreateSerializer", "ChartListSerializer", "ChartRetrieveSerializer"]


class ChartCreateSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Chart
        exclude = ["data"]
        read_only_fields = ["id", "status", "created", "updated"]


class ChartListSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Chart
        exclude = ["data"]


class ChartRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Chart
        fields = "__all__"


class ChartUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Chart
        fields = ["name"]
