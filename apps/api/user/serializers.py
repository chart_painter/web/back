from djoser.serializers import TokenCreateSerializer as BaseTokenCreateSerializer

__all__ = ["TokenCreateSerializer"]


class TokenCreateSerializer(BaseTokenCreateSerializer):
    ...
