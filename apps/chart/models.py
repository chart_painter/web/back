from django.contrib.postgres.fields import JSONField
from django.db import models
from django.template.defaultfilters import truncatechars
from django.utils.translation import gettext as _

__all__ = ["Chart"]


class Chart(models.Model):
    class STATUS:
        CREATED = "created"
        IN_PROGRESS = "in_progress"
        FAILED = "failed"
        SUCCESS = "success"

        CHOICES = (
            (CREATED, _("Created")),
            (IN_PROGRESS, _("In progress")),
            (FAILED, _("Failed")),
            (SUCCESS, _("Success")),
        )

    name = models.CharField(_("Name"), max_length=128)
    status = models.CharField(_("Status"), max_length=16, choices=STATUS.CHOICES, default=STATUS.CREATED)
    a = models.PositiveIntegerField(_("A"), default=10)
    b = models.PositiveIntegerField(_("B"), default=10)
    c = models.PositiveIntegerField(_("C"), default=10)
    d = models.PositiveIntegerField(_("D"), default=10)
    step = models.PositiveIntegerField(_("Step"), default=1)
    max_x = models.PositiveIntegerField(_("Max X"), default=1000)

    data = JSONField(_("Chart data"), default=list, blank=True, null=True)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    updated = models.DateTimeField(_("Updated"), auto_now=True)

    def __str__(self) -> str:
        return truncatechars(self.name, 32)

    class Meta:
        app_label = "chart"
        ordering = ["-created"]
