from django.contrib import admin, messages
from django.contrib.postgres.fields import JSONField
from django.db import transaction
from django.utils.translation import gettext as _
from jsoneditor.forms import JSONEditor

from .tasks import put_chart_to_queue
from .models import Chart
from .forms import ChartAdminForm


@admin.register(Chart)
class ChartAdmin(admin.ModelAdmin):
    form = ChartAdminForm

    radio_fields = {"status": admin.HORIZONTAL}
    formfield_overrides = {JSONField: {"widget": JSONEditor}}
    list_display = ["name", "status", "a", "b", "c", "d", "step", "max_x", "created", "updated"]
    list_filter = ["status", "created", "updated"]
    search_fields = ["name"]
    readonly_fields = ["created", "updated"]

    fieldsets = (
        (_("Service"), {"fields": ("will_push",)}),
        (_("Main"), {"fields": ("name", "status")}),
        (_("Params"), {"fields": (("a", "b", "c", "d"), ("step", "max_x"))}),
        (_("Raw result"), {"classes": ("collapse",), "fields": ("data",)}),
        (None, {"fields": ("created", "updated")}),
        ("Chart", {"fields": ("chart",)}),
    )

    class Media:
        css = {"all": ("vendors/apexcharts/apexcharts.css",)}
        js = ("vendors/apexcharts/apexcharts.js", "js/admin-chart.js")

    def save_model(self, request, obj: Chart, form: ChartAdminForm, change) -> None:
        obj.save()

        if form.cleaned_data['will_push']:
            with transaction.atomic():
                try:
                    transaction.on_commit(lambda: put_chart_to_queue.delay(obj.pk))
                    messages.success(request, f"Pushed chart '{obj.name}' to queue was successful")
                except Exception as e:
                    messages.warning(request, f"Pushed chart '{obj.name}' to queue was failure")

                    raise e
