import json
import logging

import pika
from celery.task import task
from django.conf import settings
from django.forms import model_to_dict

from apps.chart.models import Chart

logger = logging.getLogger(__name__)


@task(ignore_result=True, queue="charts")
def put_chart_to_queue(chart_id: int) -> None:
    try:
        instance = Chart.objects.get(pk=chart_id)
    except Chart.DoesNotExist as e:
        logger.error(f"ChartID={chart_id} does not exists")

        raise e

    payload = model_to_dict(instance, fields=["id", "a", "b", "c", "d", "step", "max_x"])
    payload = json.dumps(payload)

    try:
        parameters = pika.URLParameters(settings.AMQP_DSN)
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        properties = pika.BasicProperties(content_type="text/json", delivery_mode=1)
        channel.basic_publish(settings.AMQP_EXCHANGE, settings.AMQP_ROUTING_KEY, payload, properties)
        connection.close()

        instance.status = Chart.STATUS.SUCCESS

        logger.info(f"ChartID={chart_id} was pushed to queue")
    except Exception as e:
        instance.status = Chart.STATUS.FAILED

        logger.exception(f"Error at pushing ChartID={chart_id} to queue")

        raise e
    finally:
        instance.save(update_fields=["status", "updated"])
