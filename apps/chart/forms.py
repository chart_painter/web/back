from django import forms
from django.utils.translation import gettext as _

from .models import Chart

__alL__ = ["ChartAdminForm"]


class ChartAdminForm(forms.ModelForm):
    will_push = forms.BooleanField(label=_("Will push to queue?"), initial=True, required=False)
    chart = forms.CharField(required=False)

    class Meta:
        model = Chart
        fields = '__all__'
