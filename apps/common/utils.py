from typing import Any

__all__ = ["get_object_or_none"]


def get_object_or_none(model: Any, *args, **kwargs) -> Any:
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None
