import io

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User as BaseUser
from django.contrib.sessions.models import Session
from django.test import TestCase
from django_dynamic_fixture import G
from PIL import Image

from apps.common.utils import get_object_or_none

__all__ = ["BaseTest"]

User: BaseUser = get_user_model()


class BaseTest(TestCase):
    maxDiff = 5000

    EMAIL = "user@example.com"
    PASSWORD = "123-123-123"

    user: User

    def setUp(self):
        super(BaseTest, self).setUp()

        self.create_user_instance()

    def get_image(self) -> io.BytesIO:
        file = io.BytesIO()
        image = Image.new("RGBA", size=(1, 1))
        image.save(file, "png")
        file.name = "test.png"
        file.seek(0)

        return file

    def create_user_instance(self) -> None:
        self.user: User = G(User, email=self.EMAIL, is_active=True)
        self.user.set_password(self.PASSWORD)
        self.user.save()

    def reload_user_instance(self) -> None:
        self.user: User = get_object_or_none(User, pk=self.user.pk)

    def update_user_instance(self, **kwargs):
        self.user.__dict__.update(kwargs)
        self.user.save()

    def assertUserPassword(self, user: User, password: str, will_right=True):
        is_right = user.check_password(password)
        if is_right != will_right:
            if will_right:
                self.fail(f'Password "{password}" is not right for User ID={user.pk}')
            else:
                self.fail(f'Password "{password}" is right User ID={user.pk}')

    def assertSessionid(self, sessionid: str, user_to_compare: User = None) -> None:
        session = get_object_or_none(Session, session_key=sessionid)
        if session is None:
            self.fail(f"Session was not found by sessionid={sessionid}")

        uid = session.get_decoded().get("_auth_user_id")
        user: User = get_object_or_none(User, id=uid)
        if user is None:
            self.fail(f"User was not found by sessionid={sessionid}")

        if user_to_compare is not None:
            if user_to_compare.pk != user.pk:
                self.fail(f"Users ID's are not equal: {user.pk} != {user_to_compare.pk}")
